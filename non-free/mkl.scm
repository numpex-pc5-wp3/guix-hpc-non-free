;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; However, note that this module provides packages for "non-free" software,
;;; which denies users the ability to study and modify it.  These packages
;;; are detrimental to user freedom and to proper scientific review and
;;; experimentation.  As such, we kindly invite you not to share it.
;;;
;;; Copyright © 2019, 2023 Inria

(define-module (non-free mkl)
  #:use-module (guix packages)
  #:use-module (guix deprecation)
  #:use-module (guix-science-nonfree packages mkl))

(define-deprecated/public mkl
  (deprecated-package "mkl" intel-oneapi-mkl))
