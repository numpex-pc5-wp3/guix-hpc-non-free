;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; However, note that this module provides packages for "non-free" software,
;;; which denies users the ability to study and modify it.  These packages
;;; are detrimental to user freedom and to proper scientific review and
;;; experimentation.  As such, we kindly invite you not to share it.
;;;
;;; Copyright © 2023, 2024 Inria

(define-module (tainted python-xyz)
  #:use-module (guix)
  #:use-module (gnu packages python-xyz)
  #:use-module (guix-science-nonfree packages mkl))

(define-public python-numpy-mkl
  (package/inherit python-numpy
    (name "python-numpy-mkl")
    (arguments
     (substitute-keyword-arguments (package-arguments python-numpy)
       ((#:phases phases #~%standard-phases)
        #~(modify-phases #$phases
            (replace 'configure-blas
              (lambda* (#:key inputs #:allow-other-keys)
                ;; See <https://github.com/numpy/numpy/blob/main/site.cfg.example>.
                (call-with-output-file "site.cfg"
                  (lambda (port)
                    (format port
                            "\
[mkl]
libraries = mkl_rt
library_dirs = ~a/lib
include_dirs = ~:*~a/include~%"
                            (dirname (dirname
                                      (search-input-file
                                       inputs "include/mkl.h"))))))))))
       ((#:tests? _ #t)
        ;; FIXME: Tests run until we get, at around 95% completion:
        ;;   node down: Not properly terminated
        ;;   maximum crashed workers reached: 120
        ;; All subsequent tests fail.
        #f)))
    (inputs (modify-inputs (package-inputs python-numpy)
              (replace "openblas" intel-oneapi-mkl)))
    (synopsis
     "Fundamental package for scientific computing with Python (MKL build)")))
