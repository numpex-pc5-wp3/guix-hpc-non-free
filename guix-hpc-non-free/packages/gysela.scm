;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Copyright © 2024 Inria

(define-module (guix-hpc-non-free packages gysela)
  #:use-module ((guix licenses)
                #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix utils)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix-hpc packages gysela)
  #:use-module (guix-hpc-non-free packages cpp)
  #:use-module (guix-hpc-non-free packages utils)
  #:use-module (guix-science-nonfree packages cuda))

(define (make-gyselalibxx-cuda cuda-arch cuda-package kokkos-package
                               ginkgo-package)
  ;; FIXME: check that the different CUDA versions are compatible
  (package/inherit gyselalibxx
    (name (string-append "gyselalibxx-cuda-" cuda-arch))
    (arguments (substitute-keyword-arguments (package-arguments gyselalibxx)
                 ((#:configure-flags flags)
                  #~(append (list "-DKokkos_ENABLE_CUDA=ON"
                                  ;; deactivate testing as building
                                  ;; vendored ddc fails with tests
                                  "-DBUILD_TESTING=OFF")
                            #$flags))
                 ;; Cannot run tests due to lack of specific hardware
                 ((#:tests? _ #f)
                  #f)
                 ;; RUNPATH validation fails since libcuda.so.1 is not
                 ;; present at build time.
                 ((#:validate-runpath? #f #f)
                  #f)))
    (inputs (modify-inputs (package-inputs gyselalibxx)
              (prepend cuda-package)
              (replace "kokkos" kokkos-package)
              (replace "ginkgo" ginkgo-package)))))

(define-public gyselalibxx-cuda-k40
  ;; K40 architecture is not supported by CUDA 12
  (make-gyselalibxx-cuda "k40" cuda-11 kokkos-cuda-k40 ginkgo-cuda-11))

(define-public gyselalibxx-cuda-a100
  (make-gyselalibxx-cuda "a100" cuda kokkos-cuda-a100 ginkgo-cuda-12))

(define-public gyselalibxx-cuda-v100
  (make-gyselalibxx-cuda "v100" cuda kokkos-cuda-v100 ginkgo-cuda-12))

(define-public gyselalibxx-cuda-p100
  (make-gyselalibxx-cuda "p100" cuda kokkos-cuda-p100 ginkgo-cuda-12))
