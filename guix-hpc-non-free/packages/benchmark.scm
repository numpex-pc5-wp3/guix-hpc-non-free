;;; This module extends GNU Guix and is licensed under the same terms, those
;;; of the GNU GPL version 3 or (at your option) any later version.
;;;
;;; Note that this module provides packages that depend on "non-free"
;;; software, which denies users the ability to study and modify it.
;;;
;;; Copyright © 2024 Inria

(define-module (guix-hpc-non-free packages benchmark)
  #:use-module (guix)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix git)		;for 'git-checkout'
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix-hpc packages benchmark)
  #:use-module (guix-science-nonfree packages cuda)
  #:use-module (guix-science-nonfree packages mpi))

(define-public osu-micro-benchmarks-cuda
  (package/inherit osu-micro-benchmarks
    (name (string-append (package-name osu-micro-benchmarks) "-cuda"))
    ;; The arguments (in particular the configure flags) and the
    ;; inputs are not derived from the parent package, as the input
    ;; substitution in the configure flags definition doesn't take
    ;; into account the substitution in the inputs.
    (arguments
     (list #:configure-flags
           #~(list (string-append "CC="
                                  #$(this-package-input "openmpi-cuda")
                                  "/bin/mpicc")
                   (string-append "CXX="
                                  #$(this-package-input "openmpi-cuda")
                                  "/bin/mpicxx")
                   "--enable-cuda"
                   (string-append "--with-cuda-include="
                                  #$(this-package-input "cuda-toolkit")
                                  "/include")
                   (string-append "--with-cuda-libpath="
                                  #$(this-package-input "cuda-toolkit")
                                  "/lib/stubs")
                   "--enable-ncclomb"
                   (string-append "--with-nccl="
                                  #$(this-package-input "nccl")))
           #:validate-runpath? #f))
    (inputs (list cuda
                  openmpi-cuda
                  nccl))
    (synopsis "MPI microbenchmarks with CUDA support.")
    (description "A collection of host-based and device-based microbenchmarks for MPI
communication with CUDA support.")))
